use </usr/share/openscad/libraries/MCAD/libtriangles.scad>

module outerpoly() {
    points = [
        [0,0,0],    // 0
        [36,0,0],   // 1
        [36,42,0],  // 2
        [0,42,0],   // 3
        [8,0,16],  // 4
        [28,0,16],  // 5
        [28,42,16], // 6
        [8,42,16]]; // 7
    faces = [
        [0,1,2,3],  // bottom
        [4,5,1,0],  // front
        [7,6,5,4],  // top
        [5,6,2,1],  // right
        [6,7,3,2],  // back
        [7,4,0,3]]; // left
    polyhedron(points, faces);
}
module innerpoly() {
    points = [
        [0,0,0],    // 0
        [32,0,0],   // 1
        [32,38,0],  // 2
        [0,38,0],   // 3
        [8,0,14],  // 4
        [24,0,14],  // 5
        [24,38,14], // 6
        [8,38,14]]; // 7
    faces = [
        [0,1,2,3],  // bottom
        [4,5,1,0],  // front
        [7,6,5,4],  // top
        [5,6,2,1],  // right
        [6,7,3,2],  // back
        [7,4,0,3]]; // left
    translate([2,2,0]) polyhedron(points, faces);
}
module hole() {
    cylinder(2,7,7);
}
module backwindow() {
    points = [
        [0,0,0],    // 0
        [27,0,0],   // 1
        [27,2,0],  // 2
        [0,2,0],   // 3
        [6,0,10],  // 4
        [19,0,10],  // 5
        [19,2,10], // 6
        [6,2,10]]; // 7
    faces = [
        [0,1,2,3],  // bottom
        [4,5,1,0],  // front
        [7,6,5,4],  // top
        [5,6,2,1],  // right
        [6,7,3,2],  // back
        [7,4,0,3]]; // left
    polyhedron(points, faces);
} //backwindow();

module sidewindow() {
    cube([10,30,4]);
    translate([4,0,6]) cube([3,30,10]);
} // sidewindow();

module lateralgrab() {
    points = [
        [0,0,0],    // 0
        [2,0,0],   // 1
        [2,38,0],  // 2
        [0,38,0],   // 3
        [1,0,2],  // 4
        [3,0,2],  // 5
        [3,38,2], // 6
        [1,38,2]]; // 7
    faces = [
        [0,1,2,3],  // bottom
        [4,5,1,0],  // front
        [7,6,5,4],  // top
        [5,6,2,1],  // right
        [6,7,3,2],  // back
        [7,4,0,3]]; // left
    polyhedron(points, faces);
} //lateralgrab();

module case() {
    difference() {
        outerpoly();
        innerpoly();
        translate([19, 12, 14]) hole();
        translate([7,0,0]) backwindow();
        translate([0,6,0]) sidewindow();
    }
} //case();

module screwholder() {
    difference() {
        difference() {
            cylinder(4,8,8);
            translate([0,0,2]) cylinder(2,7,7);
        }
        cylinder(2,2.5,2.5);
    }
} //screwholder();

module insidegrab() {
    difference() {
        cube([7,12,14]);
        translate([0,0,3.5]) cube([7,8,4]);
        }
} //insidegrab();

module finger() {
    points = [
        [0,0,0],    // 0
        [1,11,0],   // 1
        [4,20,0],   // 2
        [15,22,0],  // 3
        [15,0,0],   // 4
        [0,0,10],   // 5
        [10,0,9],   // 6
        [15,0,0],   // 7
        [0,0,10],   // 8
        [1,11,8.25],// 9
        [4,20,6.5], // 10
        [12,21,5.5], // 11
        [4,20,6.5]  // 12
    ];
    faces = [
        [4,3,2],[4,2,1],[4,1,0],            // base
        [0,1,8],[1,9,8],[1,10,9],[1,2,10],  // B
        [2,3,11],[2,11,12],                 // C
        [3,6,11],[3,4,6],                   // D
        [6,8,9],[6,9,10],[6,10,11]          // top
    ];
    polyhedron(points, faces);
} //finger();

module final() {
    union() {
        case();
        translate([19, 12, 10]) screwholder();
        translate([4,2,4]) lateralgrab();
        translate([21,30,14]) rotate([0,180,0])
            insidegrab();
        translate([26,42,10]) rotate([0,195,0]) finger();
    }
}
final();
